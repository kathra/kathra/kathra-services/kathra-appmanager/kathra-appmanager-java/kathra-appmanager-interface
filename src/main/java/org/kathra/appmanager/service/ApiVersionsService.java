/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.service;

import org.kathra.core.model.ApiVersion;
import javax.activation.FileDataSource;

public interface ApiVersionsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Create a new ApiVersion via an OpenAPI file
    * 
    * @param componentId The ID of the Component the APIVersion will be created for (required)
    * @param openApiFile The OpenAPI file representing the APIVersion to create (optional)
    * @return ApiVersion
    */
    ApiVersion createApiVersion(String componentId, FileDataSource openApiFile) throws Exception;

    /**
    * Delete ApiVersion via an OpenAPI file
    * 
    * @param apiVersionId The ID of the Component the APIVersion will be deleted for (required)
    * @return ApiVersion
    */
    ApiVersion deleteApiVersion(String apiVersionId) throws Exception;

    /**
    * Get the API File from the specified source repository
    * 
    * @param apiVersionId The ID of the requested APIVersion file (required)
    * @return FileDataSource
    */
    FileDataSource getApiFile(String apiVersionId) throws Exception;

    /**
    * Get the Api Version having the given id
    * 
    * @param apiVersionId The ID of the Api Version to get (required)
    * @return ApiVersion
    */
    ApiVersion getApiVersionById(String apiVersionId) throws Exception;

    /**
    * Update ApiVersion via an OpenAPI file
    * 
    * @param apiVersionId The ID of the Component the APIVersion will be created for (required)
    * @param openApiFile The OpenAPI file representing the APIVersion to create (optional)
    * @return ApiVersion
    */
    ApiVersion updateApiVersion(String apiVersionId, FileDataSource openApiFile) throws Exception;

}
