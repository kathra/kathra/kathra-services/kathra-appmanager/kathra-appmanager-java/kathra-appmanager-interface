/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.service;

import org.kathra.core.model.CatalogEntry;
import org.kathra.core.model.CatalogEntryPackage;
import org.kathra.appmanager.model.CatalogEntryTemplate;
import java.util.List;

public interface CatalogEntriesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new entry to catalog from template
    * 
    * @param catalogEntry The catalog entry to create from template (optional)
    * @return CatalogEntry
    */
    CatalogEntry addEntryToCatalogFromTemplate(CatalogEntryTemplate catalogEntry) throws Exception;

    /**
    * Delete the CatalogEntry having the given id
    * 
    * @param catalogEntryId The ID of the CatalogEntry to delete (required)
    * @return CatalogEntry
    */
    CatalogEntry deleteCatalogEntryById(String catalogEntryId) throws Exception;

    /**
    * Get the catalog entries list
    * 
    * @return List<CatalogEntry>
    */
    List<CatalogEntry> getCatalogEntries() throws Exception;

    /**
    * Get the CatalogEntry having the given id
    * 
    * @param catalogEntryId The ID of the CatalogEntryId to get (required)
    * @return CatalogEntry
    */
    CatalogEntry getCatalogEntry(String catalogEntryId) throws Exception;

    /**
    * Get the CatalogEntryPackage having the given ProviderID [latest version]
    * 
    * @param providerId The ProviderId of the CatalogEntryPackage to get (required)
    * @return CatalogEntryPackage
    */
    CatalogEntryPackage getCatalogEntryPackageFromProviderId(String providerId) throws Exception;

    /**
    * Get the CatalogEntryPackage having the given ProviderID and version
    * 
    * @param providerId The ProviderId of the CatalogEntryPackage to get (required)
    * @param version The Version of the CatalogEntryPackage to get (required)
    * @return CatalogEntryPackage
    */
    CatalogEntryPackage getCatalogEntryPackageFromProviderIdAndVersion(String providerId, String version) throws Exception;

    /**
    * Get all CatalogEntryPackage with ProviderID
    * 
    * @return List<CatalogEntryPackage>
    */
    List<CatalogEntryPackage> getCatalogEntryPackages() throws Exception;

    /**
    * Get codegen templates
    * 
    * @return List<CatalogEntryTemplate>
    */
    List<CatalogEntryTemplate> getCatalogEntryTemplates() throws Exception;

}
