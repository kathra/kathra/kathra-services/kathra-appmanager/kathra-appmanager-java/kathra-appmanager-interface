/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.service;

import org.kathra.core.model.Component;
import java.util.List;

public interface ComponentsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Create a new Component
    * 
    * @param component The parameters for the component to create. Required: name and metadata.groupPath (optional)
    * @return Component
    */
    Component createComponent(Component component) throws Exception;

    /**
    * Delete the component having the given id
    * 
    * @param componentId The ID of the Component to delete (required)
    * @return Component
    */
    Component deleteComponentById(String componentId) throws Exception;

    /**
    * Get the component having the given id
    * 
    * @param componentId The ID of the Component to get (required)
    * @return Component
    */
    Component getComponentById(String componentId) throws Exception;

    /**
    * Get the components list
    * 
    * @return List<Component>
    */
    List<Component> getComponents() throws Exception;

}
