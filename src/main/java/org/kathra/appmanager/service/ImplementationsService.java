/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.service;

import org.kathra.core.model.Implementation;
import org.kathra.appmanager.model.ImplementationParameters;
import java.util.List;

public interface ImplementationsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Create a new Implementation for the given ApiVersion
    * 
    * @param implementationParameters The parameters for the implementation to create (optional)
    * @return Implementation
    */
    Implementation createImplementation(ImplementationParameters implementationParameters) throws Exception;

    /**
    * Delete the Implementation having the given id
    * 
    * @param implementationId The ID of the Implementation to delete (required)
    * @return Implementation
    */
    Implementation deleteImplementationById(String implementationId) throws Exception;

    /**
    * Get the component having the given id
    * 
    * @param componentId The ID of the Component to get the implementations from (required)
    * @return List<Implementation>
    */
    List<Implementation> getComponentImplementations(String componentId) throws Exception;

    /**
    * Get the Implementation having the given id
    * 
    * @param implementationId The ID of the Implementation to get (required)
    * @return Implementation
    */
    Implementation getImplementationById(String implementationId) throws Exception;

    /**
    * Get the Implementations list
    * 
    * @return List<Implementation>
    */
    List<Implementation> getImplementations() throws Exception;

}
