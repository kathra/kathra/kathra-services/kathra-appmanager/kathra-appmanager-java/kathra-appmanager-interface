/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.service;

import org.kathra.appmanager.model.Commit;
import java.util.List;

public interface RepositoriesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Get the branches for the given SourceRepository
    * 
    * @param sourceRepositoryId The ID of the SourceRepository to get (required)
    * @return List<String>
    */
    List<String> getRepositoryBranches(String sourceRepositoryId) throws Exception;

    /**
    * Get the commits for the given SourceRepository for a given branch
    * 
    * @param sourceRepositoryId The ID of the SourceRepository to get the commits from (required)
    * @param branch The name of the branch to get the commits from (required)
    * @return List<Commit>
    */
    List<Commit> getRepositoryCommitsForBranch(String sourceRepositoryId, String branch) throws Exception;

}
